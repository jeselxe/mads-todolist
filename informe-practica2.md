## Informe Test Driven Development

La metodología de trabajo TDD es totalmente distinto a la manera que estaba siguiendo normalmente en el desarrollo de software, puesto que primero solía desarrollar el código y finalmente (aunque no siempre) los tests.

Como punto a favor destacaría el rendimiento, ya que aunque es posible que se avance más despacio en el desarrollo del proyecto, es más efectivo el arreglo de errores, porque cuanto antes se encuentre, más fácil será arreglarlo.

En contra diría que es tedioso tener que hacer tests tan a menudo, ya que el progreso se ve ralentizado, sin embargo una vez que te acostrumbras a trabajar con esta metodología, y si sabes hacer correctamente los tests, seguramente a la larga nos ahorraremos tiempo de identificación y corrección de bugs.

Una cosa que no me llega a convencer es lo de crear el test antes del código, personalmente prefiero escribir el código primero y después el test. Es decir, crear una función y justamente después el test, no escribir todo el código y después todos los test, utilizando el estilo iterativo de TDD pero inverso.

En definitiva, TDD es una buena metodología de trabajo, pero es necesario cambiar la mentalidad para poder aplicarla correctamente.

*Jesús Gallego Irles*
