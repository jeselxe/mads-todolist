package models;

import play.*;
import play.mvc.*;
import play.db.jpa.*;
import java.util.List;
import java.util.Date;

public class TareaDAO {

    public static Tarea find(Integer id) {
        return JPA.em().find(Tarea.class, id);
    }

    public static Tarea create(Tarea tarea) {
        JPA.em().persist(tarea);
        // Hacemos un flush y un refresh para asegurarnos de que se realiza
        // la creación en la BD y se devuelve el id inicializado
        JPA.em().flush();
        JPA.em().refresh(tarea);
        Logger.debug(tarea.toString());
        return tarea;
    }

    public static void delete(Integer id) {
        Tarea tarea = JPA.em().getReference(Tarea.class, id);
        JPA.em().remove(tarea);
        JPA.em().flush();
    }

    public static Tarea update(Tarea tarea) {
        Tarea t = JPA.em().merge(tarea);
        JPA.em().flush();
        return t;
    }

}
