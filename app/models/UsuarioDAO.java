package models;

import play.*;
import play.mvc.*;
import play.db.jpa.*;
import java.util.List;
import java.util.Date;

public class UsuarioDAO {
    public static Usuario create (Usuario usuario) {
        usuario.nulificaAtributos();
        JPA.em().persist(usuario);
        // Hacemos un flush y un refresh para asegurarnos de que se realiza
        // la creación en la BD y se devuelve el id inicializado
        JPA.em().flush();
        JPA.em().refresh(usuario);
        Logger.debug(usuario.toString());
        return usuario;
    }

    public static Usuario update (Usuario usuario) {
        return JPA.em().merge(usuario);
    }

    public static void delete(Integer id) {
        Usuario usuario = JPA.em().getReference(Usuario.class, id);
        JPA.em().remove(usuario);
    }

    public static List<Usuario> findAll() {
        return (List<Usuario>) JPA.em().createQuery("select u from Usuario u ORDER BY id").getResultList();
    }

    public static Usuario find(Integer id) {
        return JPA.em().find(Usuario.class, id);
    }

    public static boolean login(Usuario user) {
        List<Usuario> result = (List<Usuario>) JPA.em().createQuery("select u from Usuario u WHERE login = '" + user.login + "' AND password = '" + user.password + "'").getResultList();
        return (result.size() == 1);
    }
}
