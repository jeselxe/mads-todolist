package models;

import play.*;
import play.mvc.*;
import play.db.jpa.*;
import java.util.List;
import java.util.Date;

public class UsuarioService {

    public static Usuario grabaUsuario(Usuario usuario) {
       return UsuarioDAO.create(usuario);
   }

    public static Usuario modificaUsuario(Usuario usuario) {
       return UsuarioDAO.update(usuario);
   }

    public static boolean deleteUsuario(Integer id) {
        try {
           UsuarioDAO.delete(id);
       }
       catch(Exception ex) {
           return false;
       }
       return true;
   }

   public static List<Usuario> findAllUsuarios() {
       return UsuarioDAO.findAll();
   }

   public static Usuario findUsuario(Integer id) {
       return UsuarioDAO.find(id);
   }

   public static boolean loginUsuario(Usuario usuario) {
       return UsuarioDAO.login(usuario);
   }

}
