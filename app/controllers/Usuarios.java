package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import static play.libs.Json.*;
import play.data.Form;
import play.db.jpa.*;

import views.html.*;
import models.*;

public class Usuarios extends Controller {

    public Result formularioNuevoUsuario() {
        return ok(formCreacionUsuario.render(Form.form(Usuario.class),""));
    }

    @Transactional
    public Result editarUsuario(Integer id) {
        Usuario usuario = UsuarioService.findUsuario(id);
        Form<Usuario> form = Form.form(Usuario.class);
        form = form.fill(usuario);
        return ok(formModificacionUsuario.render(form,""));
    }

    @Transactional(readOnly = true)
    public Result listaUsuarios() {
        // Obtenemos el mensaje flash guardado en la petición
        // por el controller grabaUsuario
        String mensaje = flash("grabaUsuario");

        if(mensaje == null) {
            mensaje = flash("borraUsuario");
        }

        List<Usuario> usuarios = UsuarioService.findAllUsuarios();
        return ok(listaUsuarios.render(usuarios, mensaje));
    }

    @Transactional(readOnly = true)
    public Result detalleUsuario(Integer id) {
        Usuario usuario = UsuarioService.findUsuario(id);
        return ok(detalleUsuario.render(usuario, ""));
    }

    @Transactional
    public Result borraUsuario(Integer id) {

        Usuario user = UsuarioService.findUsuario(id);

        if(user == null){
            return badRequest();
        }

        boolean borrado = UsuarioService.deleteUsuario(id);

        String msg = borrado ? "El usuario se ha borrado correctamente" : "No se ha podido borrar el usuario";

        flash("borraUsuario", msg);

        return ok();
    }

    @Transactional
   // Añade un nuevo usuario en la BD y devuelve código HTTP
   // de redirección a la página de listado de usuarios
   public Result grabaNuevoUsuario() {
       Form<Usuario> usuarioForm = Form.form(Usuario.class).bindFromRequest();
       if (usuarioForm.hasErrors()) {

           return badRequest(formCreacionUsuario.render(usuarioForm, "Hay errores en el formulario"));
       }
       Usuario usuario = usuarioForm.get();
       usuario = UsuarioService.grabaUsuario(usuario);
       flash("grabaUsuario", "El usuario se ha grabado correctamente");
       return redirect(controllers.routes.Usuarios.listaUsuarios());
   }

    @Transactional
   // Añade un nuevo usuario en la BD y devuelve código HTTP
   // de redirección a la página de listado de usuarios
   public Result grabaUsuarioModificado() {
       Form<Usuario> usuarioForm = Form.form(Usuario.class).bindFromRequest();
       if (usuarioForm.hasErrors()) {

           return badRequest(formModificacionUsuario.render(usuarioForm, "Hay errores en el formulario"));
       }
       Usuario usuario = usuarioForm.get();
       usuario = UsuarioService.modificaUsuario(usuario);
       flash("grabaUsuario", "El usuario se ha editado correctamente");
       return redirect(controllers.routes.Usuarios.listaUsuarios());
   }

}
