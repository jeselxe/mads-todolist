package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import static play.libs.Json.*;
import play.data.Form;
import play.db.jpa.*;

import views.html.*;
import models.*;

public class Acceso extends Controller {

    public Result showLogin() {
        return ok(login.render(Form.form(Usuario.class), ""));
    }

    @Transactional(readOnly = true)
    public Result login() {
        Form<Usuario> loginForm = Form.form(Usuario.class).bindFromRequest();
        if (loginForm.hasErrors()) {

            return badRequest(login.render(loginForm, "Hay errores en el formulario"));
        }
        Usuario usuario = loginForm.get();

        if (usuario.login.equals("admin") && usuario.password.equals("admin")) {
            return redirect(controllers.routes.Usuarios.listaUsuarios());
        }
        else {
            boolean logged = UsuarioService.loginUsuario(usuario);

            if (!logged) {
                return badRequest(login.render(loginForm, "Usuario o password incorrectos"));
            }
            return redirect(controllers.routes.Public.loggedIn());
        }
    }

    public  Result formularioRegistro() {
        return ok(formRegistro.render(Form.form(Usuario.class), ""));
    }

    @Transactional
    public Result registrarUsuario() {
        Form<Usuario> registroForm = Form.form(Usuario.class).bindFromRequest();
        if (registroForm.hasErrors()) {

            return badRequest(formRegistro.render(registroForm, "Hay errores en el formulario"));
        }
        Usuario usuario = registroForm.get();
        usuario = UsuarioService.grabaUsuario(usuario);
        flash("grabaUsuario", "El usuario se ha registrado correctamente");
        return redirect(controllers.routes.Public.loggedIn());
    }

}
