package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import static play.libs.Json.*;
import play.data.Form;
import play.db.jpa.*;

import views.html.*;
import models.*;

public class Public extends Controller {

    public Result loggedIn() {
        return ok(loggedIn.render());
    }

}
