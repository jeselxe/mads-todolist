package controllers;

import java.util.List;

import play.*;
import play.mvc.*;
import static play.libs.Json.*;
import play.db.jpa.*;
import play.data.Form;

import views.html.*;
import models.*;

public class Tareas extends Controller {

    @Transactional(readOnly=true)
    public Result listaTareas(Integer usuarioId) {
        List<Tarea> tareas = TareaService.findAllTareasUsuario(usuarioId);

        String mensaje = flash("grabaTarea");

        if (mensaje == null) {
            mensaje = flash("borraTarea");
        }

        return ok(listaTareas.render(tareas, usuarioId, mensaje));
    }

    public Result formularioNuevaTarea(Integer usuarioId) {
        return ok(formCreacionTarea.render(Form.form(Tarea.class), usuarioId, ""));
    }

    @Transactional
   // Añade un nuevo usuario en la BD y devuelve código HTTP
   // de redirección a la página de listado de usuarios
   public Result grabaNuevaTarea(Integer usuarioId) {
       Form<Tarea> tareaForm = Form.form(Tarea.class).bindFromRequest();
       if (tareaForm.hasErrors()) {

           return badRequest(formCreacionTarea.render(tareaForm, usuarioId, "Hay errores en el formulario"));
       }
       Tarea tarea = tareaForm.get();
       tarea.usuario = UsuarioService.findUsuario(usuarioId);
       tarea = TareaService.grabaTarea(tarea);

       flash("grabaTarea", "La tarea se ha grabado correctamente");

       return redirect(controllers.routes.Tareas.listaTareas(usuarioId));
   }

   @Transactional
   public Result editarTarea(Integer id, Integer tareaId) {
       Tarea tarea = TareaService.findTarea(tareaId);
       Form<Tarea> form = Form.form(Tarea.class);
       form = form.fill(tarea);
       return ok(formModificacionTarea.render(form, id, ""));
   }

   @Transactional
   public Result grabaTareaModificada(Integer id) {
       Form<Tarea> tareaForm = Form.form(Tarea.class).bindFromRequest();

       if (tareaForm.hasErrors()) {

           return badRequest(formModificacionTarea.render(tareaForm, id, "Hay errores en el formulario"));
       }

       Tarea tarea = tareaForm.get();
       tarea.usuario = UsuarioService.findUsuario(id);
       tarea = TareaService.modificaTarea(tarea);

       flash("grabaTarea", "La tarea se ha editado correctamente");

       return redirect(controllers.routes.Tareas.listaTareas(id));
   }

   @Transactional
   public Result borraTarea(Integer id, Integer tareaId) {
       Tarea tarea = TareaService.findTarea(tareaId);
       Usuario user = UsuarioService.findUsuario(id);

       if(tarea == null){
           return notFound();
       }

       if(!user.tareas.contains(tarea)) {
           return badRequest();
       }

       boolean borrado = TareaService.borraTarea(tareaId);
       if (borrado)
        flash("borraTarea", "La tarea se ha borrado correctamente");

       return ok();
   }



}
