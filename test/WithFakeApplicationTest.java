import org.junit.*;
import play.test.*;
import play.Application;
import play.mvc.*;
import static play.test.Helpers.*;
import static org.junit.Assert.*;

import play.db.jpa.*;
import java.util.List;
import models.*;

public class WithFakeApplicationTest {

    @Test
    public void testfindAllUsuariosDevuelveListaVacia() {
        running (fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                List<Usuario> listaUsuarios = UsuarioService.findAllUsuarios();
                assertTrue(listaUsuarios.isEmpty());
            });
        });
    }

    @Test
    public void testfindAllUsuariosDevuelveUnUsuario() {
        running (fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                Usuario usuario = new Usuario();
                usuario.login = "Pepe";
                usuario.password = "pepe";
                UsuarioService.grabaUsuario(usuario);
                List<Usuario> listaUsuarios = UsuarioService.findAllUsuarios();
                assertTrue(listaUsuarios.size() == 1);
            });
        });
    }

    @Test
    public void testModificaUsuario() {
        running (fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                Usuario usuario = new Usuario();
                usuario.login = "Pepe";
                usuario.password = "pepe";
                usuario.apellidos = "Lopez";
                UsuarioService.grabaUsuario(usuario);

                usuario.apellidos = "Perez";

                UsuarioService.modificaUsuario(usuario);

                Usuario user = UsuarioService.findUsuario(usuario.id);
                assertEquals("Perez", user.apellidos);
            });
        });
    }

    @Test
    public void testDeleteUsuario() {
        running (fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                Usuario usuario = new Usuario();
                usuario.login = "Pepe";
                usuario.password = "pepe";
                UsuarioService.grabaUsuario(usuario);

                assertTrue(UsuarioService.deleteUsuario(usuario.id));

                List<Usuario> listaUsuarios = UsuarioService.findAllUsuarios();
                assertTrue(listaUsuarios.isEmpty());
            });
        });
    }

    @Test
    public void testLoginUsuario() {
        running (fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                Usuario usuario = new Usuario();
                usuario.login = "Pepe";
                usuario.password = "pepe";
                UsuarioService.grabaUsuario(usuario);

                assertTrue(UsuarioService.loginUsuario(usuario));
            });
        });
    }

    @Test
    public void testFindUsuario() {
        running (fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                Usuario usuario = new Usuario();
                usuario.login = "Pepe";
                usuario.password = "pepe";
                UsuarioService.grabaUsuario(usuario);

                Usuario user = UsuarioService.findUsuario(usuario.id);

                assertEquals(usuario.toString(), user.toString());
            });
        });
    }

}
