import org.junit.*;
import play.test.*;
import play.mvc.*;
import static play.test.Helpers.*;
import static org.junit.Assert.*;
import play.db.jpa.*;
import java.util.List;
import models.*;
import org.dbunit.*;

import play.libs.ws.*;

public class ListadoTareasTests extends FixtureTestCase {

    @Test
    public void testFindTareaDevuelveTarea() {
        running (app, () -> {
            JPA.withTransaction(() -> {
                Tarea tarea = TareaDAO.find(1);
                assertEquals(tarea.descripcion,"Preparar el trabajo del tema 1 de biología");
                Usuario usuario = tarea.usuario;
                assertEquals(usuario.login, "pepito");
            });
        });
    }

    @Test
    public void testTareasUsuarioDevuelveSusTareas() {
        running (app, () -> {
            JPA.withTransaction(() -> {
                Usuario usuario = UsuarioDAO.find(1);
                List<Tarea> tareas = usuario.tareas;
                assertEquals(tareas.size(), 3);
                assertTrue(tareas.contains(
                    new Tarea(usuario, "Preparar el trabajo del tema 1 de biología")));
                assertTrue(tareas.contains(
                    new Tarea(usuario, "Estudiar el parcial de matemáticas")));
                assertTrue(tareas.contains(
                    new Tarea(usuario, "Leer el libro de inglés")));
            });
        });
    }

    @Test
    public void testTareaServiceFindAllTareasDevuelveTodasLasTareas() {
        running (app, () -> {
            JPA.withTransaction(() -> {
                Integer usuarioId = 1;
                List<Tarea> tareas = TareaService.findAllTareasUsuario(usuarioId);

                Usuario pepito = new Usuario("pepito", "perez");
                assertTrue(tareas.contains(
                    new Tarea(pepito, "Preparar el trabajo del tema 1 de biología")));
                assertTrue(tareas.contains(
                    new Tarea(pepito, "Estudiar el parcial de matemáticas")));
                assertTrue(tareas.contains(
                    new Tarea(pepito, "Leer el libro de inglés")));
                });
        });
    }

    @Test
    public void testWebPaginaListadoTareas() {
        running(testServer(3333, app), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas")
                .get()
                .get(timeout);
            assertEquals(OK, response.getStatus());
            String body = response.getBody();
            assertTrue(body.contains("<h2>Listado de tareas</h2>"));
        });
    }

    @Test
    public void testWebApiListadoTareas() {
        running(testServer(3333, app), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas")
                .get()
                .get(timeout);
            assertEquals(OK, response.getStatus());
            String body = response.getBody();
            assertTrue(body.contains(
                "Preparar el trabajo del tema 1 de biología"));
            assertTrue(body.contains(
                "Estudiar el parcial de matemáticas"));
            assertTrue(body.contains(
                "Leer el libro de inglés"));
        });
    }
}
