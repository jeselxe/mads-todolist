import org.junit.*;
import play.test.*;
import play.mvc.*;
import static play.test.Helpers.*;
import static org.junit.Assert.*;
import play.db.jpa.*;
import java.util.List;
import models.*;
import org.dbunit.*;

import play.libs.ws.*;

public class CrearTareasTests extends FixtureTestCase {

    @Test
    public void testNuevaTareaDeUsuario() {
        running (app, () -> {
            JPA.withTransaction(() -> {
                Usuario user = UsuarioService.findUsuario(2);
                Tarea tarea = TareaService.grabaTarea(new Tarea(user, "Terminar la feature10"));
                List<Tarea> tareas = TareaService.findAllTareasUsuario(2);
                assertEquals(2, tareas.size());
                assertEquals("Terminar la feature10", TareaService.findTarea(tarea.id).descripcion);
            });
        });
    }

    @Test
    public void testNuevaTareaFormulario() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/usuarios/2/tareas/nueva")
                            .setFollowRedirects(true)
                            .setContentType("application/x-www-form-urlencoded")
                            .post("descripcion=feature10")
                            .get(timeout);
            assertEquals(OK, response.getStatus());
            assertTrue(response.getBody().contains("La tarea se ha grabado correctamente"));
            assertTrue(response.getBody().contains("feature10"));
        });
    }

    @Test
    public void testNuevaTareaFormularioError() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/usuarios/2/tareas/nueva")
                            .setContentType("application/x-www-form-urlencoded")
                            .post("descripcion=")
                            .get(timeout);
            assertEquals(BAD_REQUEST, response.getStatus());
            assertTrue(response.getBody().contains("Hay errores en el formulario"));
        });
    }

}
