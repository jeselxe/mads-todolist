import org.junit.*;
import play.test.*;
import play.*;
import play.mvc.*;
import static play.test.Helpers.*;
import static org.junit.Assert.*;
import play.db.jpa.*;
import java.util.List;
import models.*;
import org.dbunit.*;

import play.libs.ws.*;

public class ModificaTareasTests extends FixtureTestCase {

    @Test
    public void testEditaTareaUsuario() {
        running (fakeApplication(), () -> {
            JPA.withTransaction(() -> {
                Usuario usuario = new Usuario();
                usuario.login = "Pepe";
                usuario.password = "pepe";
                usuario.apellidos = "Lopez";
                UsuarioService.grabaUsuario(usuario);

                Tarea tarea = new Tarea(usuario, "Nueva tarea");

                TareaService.grabaTarea(tarea);

                tarea.descripcion = "Nueva tarea modificada";

                TareaService.modificaTarea(tarea);

                Tarea tareaModificada = TareaService.findTarea(tarea.id);
                assertEquals("Nueva tarea modificada", tareaModificada.descripcion);
            });
        });
    }

    @Test
    public void testModificaTareaFormulario() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas/1/editar")
                .get()
                .get(timeout);
            assertEquals(OK, response.getStatus());
            assertTrue(response.getBody().contains("Editar tarea"));
        });
    }


    @Test
    public void testModificaTareaPOST() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas/modifica")
                .setFollowRedirects(true)
                .setContentType("application/x-www-form-urlencoded")
                .post("descripcion=feature12+terminada&id=1")
                .get(timeout);
            assertEquals(OK, response.getStatus());
            String body = response.getBody();
            assertTrue(body.contains("feature12 terminada"));
            assertTrue(body.contains("Estudiar el parcial de matemáticas"));
            assertTrue(body.contains("Leer el libro de inglés"));
        });
    }

    @Test
    public void testModificaTareaButton() {
        running(testServer(3333, app), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas")
                .get()
                .get(timeout);
            assertEquals(OK, response.getStatus());
            String body = response.getBody();
            assertTrue(body.contains("<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>"));
        });
    }

}
