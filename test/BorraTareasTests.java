import org.junit.*;
import play.test.*;
import play.*;
import play.mvc.*;
import static play.test.Helpers.*;
import static org.junit.Assert.*;
import play.db.jpa.*;
import java.util.List;
import models.*;
import org.dbunit.*;

import play.libs.ws.*;

public class BorraTareasTests extends FixtureTestCase {

    @Test
    public void testBorraTareaUsuario() {
        running (app, () -> {
            JPA.withTransaction(() -> {
                boolean borrado = TareaService.borraTarea(2);
                List<Tarea> tareas = TareaService.findAllTareasUsuario(1);
                Tarea t = TareaService.findTarea(2);
                assertNull(t);
                assertTrue(borrado);
                assertEquals(2, tareas.size());
            });
        });
    }

    @Test
    public void testBorraTareaButton() {
        running(testServer(3333, app), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas")
                .get()
                .get(timeout);
            assertEquals(OK, response.getStatus());
            String body = response.getBody();
            assertTrue(body.contains("<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>"));
        });
    }

    @Test
    public void testBorraTareaUsuarioRoute() {
        running(testServer(3333, app), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas/1")
                .delete()
                .get(timeout);
            assertEquals(OK, response.getStatus());
        });
    }

    @Test
    public void testBorraTareaInexistenteRoute() {
        running(testServer(3333, app), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas/5")
                .delete()
                .get(timeout);
            assertEquals(NOT_FOUND, response.getStatus());
        });
    }

    @Test
    public void testBorraTareaDeOtroUsuarioRoute() {
        running(testServer(3333, app), () -> {
            int timeout = 4000;
            WSResponse response = WS
                .url("http://localhost:3333/usuarios/1/tareas/4")
                .delete()
                .get(timeout);
            assertEquals(BAD_REQUEST, response.getStatus());
        });
    }

}
