import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.libs.F.*;

import play.libs.ws.*;
import play.Logger;

import static play.test.Helpers.*;
import static org.junit.Assert.*;

public class WebServiceTest {

    @Test
    public void testFormularioLogin() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/login").get().get(timeout);
            assertEquals(OK, response.getStatus());
            assertTrue(response.getBody().contains("Please sign in"));
        });
    }

    @Test
    public void testdoLoginUsuarioNotFound() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/login")
                            .setContentType("application/x-www-form-urlencoded")
                            .post("login=jesus&password=gallego")
                            .get(timeout);
            assertEquals(BAD_REQUEST, response.getStatus());
            assertTrue(response.getBody().contains("Usuario o password incorrectos"));
        });
    }

    @Test
    public void testRegistraNuevoUsuario() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/register")
                            .setFollowRedirects(true)
                            .setContentType("application/x-www-form-urlencoded")
                            .post("login=domingo&password=gallardo&eMail=domingo.gallardo@ua.es")
                            .get(timeout);
            assertTrue(response.getBody().contains("Bienvenido"));
        });
    }

    @Test
    public void testFormularioNuevoUsuario() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/usuarios/nuevo").get().get(timeout);
            assertEquals(OK, response.getStatus());
            assertTrue(response.getBody().contains("Añadir nuevo usuario"));
        });
    }

    @Test
    public void testNuevoUsuario() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/usuarios/nuevo")
                            .setFollowRedirects(true)
                            .setContentType("application/x-www-form-urlencoded")
                            .post("login=jesus&password=gallego&eMail=jesusgallego@outlook.com")
                            .get(timeout);
            assertTrue(response.getBody().contains("Listado de usuarios"));
        });
    }

    @Test
    public void testNuevoUsuarioFallo() {
        running(testServer(3333), () -> {
            int timeout = 4000;
            WSResponse response = WS.url("http://localhost:3333/usuarios/nuevo")
                            .setContentType("application/x-www-form-urlencoded")
                            .post("login=jesus&eMail=jesusgallego@outlook.com")
                            .get(timeout);
            assertTrue(response.getBody().contains("Hay errores en el formulario"));
        });
    }
}
